﻿Configuration Test {

    Import-DSCResource -ModuleName xPSDesiredStateConfiguration

    Node localhost { 
        xRemoteFile iisstart{
            Uri = 'https://armstorageforvms.blob.core.windows.net/html/vm1/iisstart.htm'
            DestinationPath = 'c:\inetpub\wwwroot\iisstart.htm'
        }
    }
}
