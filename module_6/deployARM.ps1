﻿#log in to azure subscription
Login-AzureRmAccount

#Create resource group: provide name and location
$resourceGroup ="devopsarm5"
$location = "North Europe"

 #Create a new resource group is necessary 

#New-AzureRmResourceGroup -Name $resourceGroup -Location $location

#Deploy the template in the defined Resource Group
#proviide full path to the json files instead of %path%

New-AzureRmResourceGroupDeployment -Name "ARMDeploy" -ResourceGroupName $resourceGroup -TemplateFile "%path%\temp.json" -TemplateParameterFile "%path%\param.json"



