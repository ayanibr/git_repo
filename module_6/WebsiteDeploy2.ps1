﻿Configuration WebsiteDeploy2 {

    Import-DSCResource -ModuleName xPSDesiredStateConfiguration

    Node localhost { 
        xRemoteFile iisstart{
            Uri = 'https://armstorageforvms.blob.core.windows.net/html/vm2/iisstart.htm'
            DestinationPath = 'c:\inetpub\wwwroot\iisstart.htm'
        }
    }
}
